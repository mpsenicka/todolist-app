import cz.vse.java.todolist.logic.Tag;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TagTest {
    private Tag tag;

    @BeforeEach
    public void setUp() {
        tag = new Tag(1, "Oukej");
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    void tagTests() {
        assertEquals(1, tag.getId());
        assertEquals("Oukej", tag.getTitle());
    }
}