import cz.vse.java.todolist.db.DbMethods;
import cz.vse.java.todolist.logic.App;
import cz.vse.java.todolist.logic.Tag;
import cz.vse.java.todolist.logic.Todo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    private App app;
    private Tag tag;
    private Todo todo;
    private Map<Integer, Tag> tagMap;
    private Map<Integer, Todo> todoMap;


    @BeforeEach
    public void setUp() {
        app = new App();
        tag = new Tag(1, "Testik");
        todo = new Todo(1, 1, "Not okay", 0, 1);
        tagMap = new HashMap<>();
        todoMap = new HashMap<>();
        app.addTag("Testik");
        tagMap.put(1, tag);
        app.addTodo(todo.getDescription(), tag, todo.getPriority());
        todoMap.put(1, todo);
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    void appTest() {
        app.fetchTags();
        app.fetchTodos();
        assertEquals(tagMap.toString(), app.getTags().toString());
        assertEquals("Testik", app.getTag(tag.getId()).toString());
        assertEquals(1, app.getTag(tag.getId()).getId());
        assertEquals(todoMap.toString(), app.getTodos().toString());
        assertEquals("Not okay", app.getTodo(todo.getId()).toString());
        assertEquals(1, app.getTodo(todo.getId()).getId());
        DbMethods.deleteAllTags();
        DbMethods.deleteAllTodos();
        DbMethods.deleteIncrement();
        app.fetchTags();
        app.fetchTodos();
        assertEquals("{}", app.getTags().toString());
        assertEquals("{}", app.getTodos().toString());
    }
}