package cz.vse.java.todolist.utils;

public class PriorityMap {

    public static String getTranslatedPriority(Integer priorityNumber) {
        switch(priorityNumber) {
            case 1:
                return "!";
            case 2:
                return "!!";
            case 3:
                return "!!!";
            default:
                return "?";
        }
    }

    public static Integer getTranslatedReversedPriority(String priorityNumber) {
        switch(priorityNumber) {
            case "!":
                return 1;
            case "!!":
                return 2;
            case "!!!":
                return 3;
            default:
                return 0;
        }
    }

}
