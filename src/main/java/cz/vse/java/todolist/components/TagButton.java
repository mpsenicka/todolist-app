package cz.vse.java.todolist.components;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;

import java.io.IOException;

public class TagButton extends HBox {

    public TagButton() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/tags.fxml"));
        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
