package cz.vse.java.todolist.logic;

import cz.vse.java.todolist.constants.FILTER;
import cz.vse.java.todolist.constants.PRIORITY;
import cz.vse.java.todolist.constants.RESOLVED;
import cz.vse.java.todolist.db.DbMethods;

import java.util.*;
import java.util.stream.IntStream;

import cz.vse.java.todolist.main.IObserver;
import cz.vse.java.todolist.main.ISubject;

public class App implements ISubject {

    private Map<Integer, Tag> tags;
    private Map<Integer, Todo> todos;
    private String filteredBy = FILTER.NONE;
    private Tag tagToBeFilteredBy;
    private Integer priorityToBeFilteredBy;

    private Set<IObserver> observerList;

    public App() {
        tags = new HashMap<>();
        todos = new HashMap<>();
        observerList = new HashSet<>();
    }

    public Map<Integer, Tag> getTags() {
        return tags;
    }

    public Map<Integer, Todo> getTodos() {
        Map<Integer, Todo> filteredTodos = new HashMap<>();

        switch (filteredBy) {
            case FILTER.TAG: {
                if (tagToBeFilteredBy.getTitle().toUpperCase().equals(FILTER.ALL)) {
                    return todos;
                }
                for (Integer task_id : todos.keySet()) {
                    Todo currentTodo = todos.get(task_id);
                    if (currentTodo.getTagId().equals(tagToBeFilteredBy.getId())) {
                        filteredTodos.put(currentTodo.getId(), currentTodo);
                    }
                }
                return filteredTodos;
            }
            case FILTER.PRIORITY: {
                for (Integer task_id : todos.keySet()) {
                    Todo currentTodo = todos.get(task_id);
                    if (currentTodo.getPriority().equals(priorityToBeFilteredBy)) {
                        filteredTodos.put(currentTodo.getId(), currentTodo);
                    }
                }
                return filteredTodos;
            }
            default: {
                return todos;
            }
        }
    }

    public Tag getTag(Integer tagId) {
        return tags.get(tagId);
    }

    public Tag getTagByName(String tagName) {
        Map<String, Tag> reversedMap = new HashMap<>();


        for (Map.Entry<Integer, Tag> entry : tags.entrySet()) {
            if (entry.getValue().getTitle().equals(tagName)) {
                reversedMap.put(tagName, entry.getValue());
                break;
            }
        }
        return reversedMap.get(tagName);
    }

    public Todo getTodo(Integer todoId) {
        return todos.get(todoId);
    }

    public void setTags(Map<Integer, Tag> fetchedTags) {
        this.tags = fetchedTags;
        Tag allTag = new Tag(0, "All");
        tags.put(allTag.getId(), allTag);
    }

    public void setTodos(Map<Integer, Todo> todos) {
        this.todos = todos;
    }

    public void removeTag(Tag tag) {
        DbMethods.deleteTag(tag.getId());
    }

    public void removeTodo(Todo todo) {
        DbMethods.deleteTodo(todo.getId());
    }

    public void addTag(String tagName) {
        DbMethods.insertTag(tagName);
    }

    public void addAllTag(Tag tag) {
        tags.put(tag.getId(), tag);
    }

    public void addTodo(String todoDescription, Tag tag, Integer priority) {
        DbMethods.insertTodo(todoDescription, tag.getId(), priority);
    }

    public void setTodoResolved(Todo todo) {
        if (todo.isResolved()) {
            DbMethods.updateResolved(RESOLVED.NO, todo.getId());
        } else {
            DbMethods.updateResolved(RESOLVED.YES, todo.getId());
        }
    }

    public void setTodoPriority(Integer priority, Todo todo) {
        boolean contains = IntStream.of(PRIORITY.LEVELS).anyMatch(x -> x == priority);
        if (contains) {
            DbMethods.updatePriority(priority, todo.getId());
        }
    }

    public void fetchTodos() {
        setTodos(DbMethods.fetchTodos());
    }

    public void fetchTags() {
        setTags(DbMethods.fetchTags());
    }

    public void setFilteredBy(String filteredBy) {
        this.filteredBy = filteredBy;
    }

    public void setTagToBeFilteredBy(Tag tagToBeFilteredBy) {
        this.tagToBeFilteredBy = tagToBeFilteredBy;
        notifyObservers();
    }

    public void setPriorityToBeFilteredBy(Integer priorityToBeFilteredBy) {
        this.priorityToBeFilteredBy = priorityToBeFilteredBy;
        notifyObservers();
    }

    public Tag getTagToBeFilteredBy() {
        return tagToBeFilteredBy;
    }

    public String getFilteredBy() {
        return filteredBy;
    }

    public Integer getPriorityToBeFilteredBy() {
        return priorityToBeFilteredBy;
    }

    @Override
    public void register(IObserver observer) {
        observerList.add(observer);
    }

    @Override
    public void unregister(IObserver observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (IObserver observer : observerList) {
            observer.update();
        }
    }
}
