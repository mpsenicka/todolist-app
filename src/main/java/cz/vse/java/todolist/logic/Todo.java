package cz.vse.java.todolist.logic;

public class Todo {

    private Integer id;
    private Integer tagId;
    private String description;
    private Integer resolved;
    private Integer priority;

    public Todo(Integer id, Integer tagId, String description, Integer resolved, Integer priority) {
        this.id = id;
        this.tagId = tagId;
        this.description = description;
        this.resolved = resolved;
        this.priority = priority;
    }

    public Integer getPriority() {
        return priority;
    }

    public Integer getId() {
        return id;
    }

    public Integer getTagId() {
        return tagId;
    }

    public String getDescription() {
        return description;
    }

    public Boolean isResolved() {
        if (resolved == 1) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return getDescription();
    }
}
