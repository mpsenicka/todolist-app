package cz.vse.java.todolist.main.controllers;

import cz.vse.java.todolist.components.TagButton;
import cz.vse.java.todolist.constants.FILTER;
import cz.vse.java.todolist.constants.PRIORITY;
import cz.vse.java.todolist.logic.App;
import cz.vse.java.todolist.logic.Tag;
import cz.vse.java.todolist.logic.Todo;
import cz.vse.java.todolist.main.IObserver;
import cz.vse.java.todolist.utils.PriorityMap;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;


public class HomeController implements IObserver {

    TodoController todoController = new TodoController();

    @FXML
    private VBox todoList;
    @FXML
    private VBox tagList;
    @FXML
    private VBox priorityList;
    @FXML
    private ComboBox tagMenu;
    @FXML
    private ComboBox priorityMenu;
    @FXML
    private TextField tagInput;
    @FXML
    private TextField todoInput;
    @FXML
    private TabPane tabPane;


    private App app;

    @FXML
    private void initialize() {
        app = new App();

        /**
         * Inicialization methods for the app
         */
        app.register(this);
        app.fetchTodos();
        app.fetchTags();

        app.setFilteredBy(FILTER.TAG);
        app.setTagToBeFilteredBy(app.getTag(0));
        app.setPriorityToBeFilteredBy(PRIORITY.ONE);

        /**
         * Listener to see what's happening with TABS
         */
        tabPane.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number oldValue, Number newValue) {
                if (newValue.equals(1)) {
                    app.setFilteredBy(FILTER.PRIORITY);
                } else {
                    app.setFilteredBy(FILTER.TAG);
                }
                update();
            }
        });

//        app.addTag("School");
//        app.addTag("Work");
//        app.addTag("Home");
//
//        for (Integer tag_id : app.getTags().keySet()) {
//            Tag currentTag = app.getTag(tag_id);
//            app.addTodo("Hello yellow " + currentTag.getTitle(), currentTag, 1);
//        }

        System.out.println("HomeController initialized");
        update();
    }


    public void handleRemoveTodo(Todo todoToBeRemoved) {
        //TODO get these values from homefxml
        app.removeTodo(todoToBeRemoved);
    }

    //TODO get these values from homefxml
    public void handleSwitchResolveTodo(Todo todoToBeResolved) {
        app.setTodoResolved(todoToBeResolved);
    }


    public void handleAddTodo(ActionEvent event) {
        String input = String.valueOf(todoInput.getCharacters());
        Object tagName = tagMenu.getSelectionModel().getSelectedItem();
        Object priority = priorityMenu.getSelectionModel().getSelectedItem();

        if ((input.length() > 1) && (tagName != null) && (priority != null)) {
            Tag tag = app.getTagByName(String.valueOf(tagName));
            app.addTodo(input, tag, Integer.valueOf(PriorityMap.getTranslatedReversedPriority(String.valueOf(priority))));
            update();
        }
    }

    public void handleRenderTodos() {
        todoList.getChildren().clear();
        try {
            for (Todo todo : app.getTodos().values()) {
                if (todo != null) {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/todos.fxml"));
                    Parent root = (Parent) loader.load();
                    todoController = loader.getController();
                    todoController.createTodo(app, todo);
                    root.setId(todo.getId().toString());
                    todoList.getChildren().add(root);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @FXML
    void handleAddTag(ActionEvent event) {
        String input = tagInput.getCharacters().toString();
        app.addTag(input);
        update();
    }

    @FXML
    void handleRemoveTag(ActionEvent event) {
        Tag tagToBeDeleted = app.getTagToBeFilteredBy();

        if (!(tagToBeDeleted.getId() == 0)) {
            app.removeTag(tagToBeDeleted);
            app.setTagToBeFilteredBy(app.getTag(0));
        }

    }

    void handleLoadTags() {
        tagList.getChildren().clear();
        for (Integer tagKey : app.getTags().keySet()) {
            Tag tag = app.getTags().get(tagKey);
            TagButton newTagButton = new TagButton();

            Button button = new Button(tag.getTitle());
            button.getStyleClass().add("tagButton");
            if (tag.getId().equals(app.getTagToBeFilteredBy().getId())) {
                button.setStyle("-fx-text-fill: #528BFF");
            }
            button.setOnMouseClicked(event -> {
                app.setTagToBeFilteredBy(tag);
                handleLoadTags();
            });
            newTagButton.getChildren().add(button);
            tagList.getChildren().add(newTagButton);
        }
    }

    void handleLoadPriorities() {
        priorityList.getChildren().clear();
        for (Integer priority : PRIORITY.LEVELS) {
            String priorityString = priority.toString();
            TagButton newPriorityButton = new TagButton();

            Button button = new Button(PriorityMap.getTranslatedPriority(priority));
            button.getStyleClass().add("tagButton");
            if (priorityString.equals(app.getPriorityToBeFilteredBy().toString())) {
                button.setStyle("-fx-text-fill: #528BFF");
            }
            button.setOnMouseClicked(event -> {
                app.setPriorityToBeFilteredBy(priority);
            });
            newPriorityButton.getChildren().add(button);
            priorityList.getChildren().add(newPriorityButton);
        }
    }

    @FXML
    void handleLoadTagMenu() {
        tagMenu.getItems().clear();
        for (Integer tagKey : app.getTags().keySet()) {
            Tag tag = app.getTags().get(tagKey);

            tagMenu.getItems().add(tag.getTitle());
        }
    }

    @FXML
    void handleLoadPriorityMenu() {
        priorityMenu.getItems().clear();
        for (Integer priority : PRIORITY.LEVELS) {
            priorityMenu.getItems().add(PriorityMap.getTranslatedPriority(priority));
        }
    }

    @Override
    public void update() {
        app.fetchTodos();
        app.fetchTags();
        handleLoadTagMenu();
        handleLoadPriorityMenu();
        if (app.getFilteredBy().equals(FILTER.TAG)) {
            handleLoadTags();
        }

        if (app.getFilteredBy().equals(FILTER.PRIORITY)) {
            handleLoadPriorities();
        }
        handleRenderTodos();

        System.out.println("HomeController updated");
    }

}
