package cz.vse.java.todolist.main.controllers;

import cz.vse.java.todolist.logic.App;
import cz.vse.java.todolist.logic.Tag;
import cz.vse.java.todolist.logic.Todo;
import cz.vse.java.todolist.utils.PriorityMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;


public class TodoController {

    @FXML
    Text descriptionId;

    @FXML
    Button tagFxId;

    @FXML
    Button priorityId;

    @FXML
    Button deleteId;

    App appTodo;


    public void createTodo(App app, Todo todo) {

        if ((app != null) && (todo != null)) {
            appTodo = app;
            Tag tag = appTodo.getTag(todo.getTagId());

            deleteId.setOnMouseClicked(event -> {
                app.removeTodo(todo);
                app.notifyObservers();
            });


            descriptionId.setText(todo.getDescription());
            tagFxId.setText(tag.getTitle());
            priorityId.setText(PriorityMap.getTranslatedPriority(todo.getPriority()));
        }
    }

}
