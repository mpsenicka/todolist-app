BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "tag" (
	"tag_id"	INTEGER NOT NULL UNIQUE,
	"tag_name"	TEXT NOT NULL UNIQUE,
	PRIMARY KEY("tag_id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "task" (
	"task_id"	INTEGER NOT NULL UNIQUE,
	"description"	TEXT NOT NULL,
	"priority"	INTEGER DEFAULT 1,
	"resolved"	INTEGER DEFAULT 0,
	"tag_id"	INTEGER,
	PRIMARY KEY("task_id" AUTOINCREMENT),
	FOREIGN KEY("tag_id") REFERENCES "tag"("tag_id"),
	CHECK ("resolved" = 0 OR "resolved" = 1)
);
INSERT INTO "tag" VALUES (3,'Debilek');
INSERT INTO "task" VALUES (4,'Nekecej vole',1,2,3);
COMMIT;
